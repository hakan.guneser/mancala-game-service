# Mancala Game Service

This is a *Java Restful API Service* that runs a mancala game. This API allows to play 2 human players on same screen as offline.

## Game Rules

Each of the two players has his six pits in front of him. To the right of the six pits, each player has a larger pit. At the start of the game, there are six stones in each of the six round pits .

* Any Players can start first
* The player who begins with the first move picks up all the beans in any of his own six pits and sows the stones on to the right, one in each of the following pits, including his own big pit
* No stones are put in the opponents' big pit
* If the player's last stone lands in his own big pit, he gets another turn
* This can be repeated several times before it's the other player's turn.

## Getting Started

### Prerequisites
***JRE 17 must be installed before run the mvn scripts***

* [Java 17](http://www.oracle.com/technetwork/java/javase/downloads/index.html) - Programming language

### Running the tests
```
./mvnw test
```
### Running the application
```
./mvnw spring-boot:run
```
### deploy to docker
```
.\deploy-to-docker.sh
```

## API documentation

- [Swagger documentation](http://localhost:8080/swagger-ui/index.html)
- [MIRO](https://miro.com/app/board/uXjVOZP1-Q0=/?share_link_id=310331701570)
- [Postman](https://www.postman.com/collections/718e8d948eb4291b9448)

## Built With

* [Spring Boot](https://projects.spring.io/spring-boot/) - The framework used
* [Maven](https://maven.apache.org) - Dependency management
* [JUnit](https://junit.org) - Test framework
* [Swagger](https://swagger.io) - Used to generate API docs & UI
* [Mapstruct](https://mapstruct.org/) - Used to Model mapping

## Author

[Hakan Guneser](https://github.com/hakanguneser)
package com.bol.mancala.game.service.model;

public enum Player {

    PLAYER_NORTH(Board.PIT_END_INDEX / 2),
    PLAYER_SOUTH(Board.PIT_END_INDEX);

    private int homePit;

    Player(int homePit) {
        this.homePit = homePit;
    }

    public int getHomePit() {
        return homePit;
    }
}

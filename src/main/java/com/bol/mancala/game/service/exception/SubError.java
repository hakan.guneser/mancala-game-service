package com.bol.mancala.game.service.exception;

import lombok.Data;

@Data
public class SubError {
    private final String errorMessage;
    private final int errorCode;
}

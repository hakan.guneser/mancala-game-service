package com.bol.mancala.game.service.model;

import java.util.ArrayList;
import java.util.List;


public class Board {

    public static final int PIT_START_INDEX = 1;
    public static final int PIT_END_INDEX = 14;

    private List<Pit> pits;

    public Board() {
        this.pits = new ArrayList<>();
        for (int i = Board.PIT_START_INDEX; i <= Board.PIT_END_INDEX; i++) {
            this.pits.add(new Pit(i));
        }
    }
    public Board(List<Pit> pits) {
        this.pits = pits;
    }
    public Pit getPit(final int index) {
        return this.pits.get((index - 1) % Board.PIT_END_INDEX);
    }

    public List<Pit> getPits() {
        return this.pits;
    }

    public int getPlayerSideTotalBeanCount(Player player,boolean includeHouse){
        return this.getPits().stream()
                .filter(pit -> (pit.getOwner().equals(player) && (includeHouse || !pit.isHomePit())))
                .mapToInt(Pit::getBeanCount).sum();
    }


    public void clearBoardAfterAPlayerWon(){
        this.getPits().stream()
                .filter(pit -> (!pit.isHomePit()))
                .forEach(pit -> pit.setBeanCount(0));
    }

}

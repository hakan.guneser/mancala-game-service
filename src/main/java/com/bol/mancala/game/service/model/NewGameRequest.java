package com.bol.mancala.game.service.model;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Getter
@Setter
@Builder
public class NewGameRequest {
    @NotEmpty(message = "north player name field can not left blank")
    @Size(min = 3,max = 25,message = "north player name field supports min 3 max 25 characters")
    private String playerNorthName;

    @NotEmpty(message = "south player name field can not left blank")
    @Size(min = 3,max = 25,message = "south player name field supports min 3 max 25 characters")
    private String playerSouthName;
}

package com.bol.mancala.game.service.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Pit {

    private final int pitId;
    private int beanCount;

    public Pit(int pitId) {
        this.pitId = pitId;
        if (!this.isHomePit()) {
            this.beanCount = Game.BEAN_COUNT;
        }
    }

    public Pit(int pitId, int beanCount) {
        this.pitId = pitId;
        this.beanCount = beanCount;
    }

    public boolean isHomePit() {
        return this.pitId == Player.PLAYER_NORTH.getHomePit() ||
                this.pitId == Player.PLAYER_SOUTH.getHomePit();
    }

    public Player getOwner() {
        if (this.getPitId() <= Player.PLAYER_NORTH.getHomePit()) {
            return Player.PLAYER_NORTH;
        } else {
            return Player.PLAYER_SOUTH;
        }
    }

    public boolean isOpponentHomePit(final Player turn) {
        return getOpponent(turn).getHomePit() == this.pitId;
    }

    private Player getOpponent(final Player player) {
        return player.equals(Player.PLAYER_NORTH) ? Player.PLAYER_SOUTH : Player.PLAYER_NORTH;
    }
}

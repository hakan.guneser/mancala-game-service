package com.bol.mancala.game.service.service;

import com.bol.mancala.game.service.model.GameResponse;
import com.bol.mancala.game.service.model.NewGameRequest;
import com.bol.mancala.game.service.model.OngoingGameResponse;

import java.util.UUID;

public interface IGameService {

    GameResponse createGame(NewGameRequest newGameRequest);

    GameResponse getGameById(UUID gameId);

    OngoingGameResponse getOngoingGame();

    GameResponse play(UUID gameId, Integer pitId);

    GameResponse leaveGame(UUID gameId);

}

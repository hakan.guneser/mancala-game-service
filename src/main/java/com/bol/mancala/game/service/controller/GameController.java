package com.bol.mancala.game.service.controller;

import com.bol.mancala.game.service.model.Board;
import com.bol.mancala.game.service.model.GameResponse;
import com.bol.mancala.game.service.model.NewGameRequest;
import com.bol.mancala.game.service.model.OngoingGameResponse;
import com.bol.mancala.game.service.service.IGameService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.net.URI;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/game")
@RequiredArgsConstructor
@Validated
public class GameController {

    private final IGameService gameService;

    @GetMapping("/{game-id}")
    public ResponseEntity<GameResponse> getGameId(@PathVariable("game-id") final UUID gameId) {
        return ResponseEntity.ok(gameService.getGameById(gameId));
    }

    @GetMapping("/ongoing")
    public ResponseEntity<OngoingGameResponse> getOngoingGameId() {
        return ResponseEntity.ok(gameService.getOngoingGame());
    }

    @PostMapping("/create")
    public ResponseEntity<GameResponse> createNewGame(@Valid @RequestBody NewGameRequest gameRequest) {
        GameResponse createdGame = gameService.createGame(gameRequest);
        return ResponseEntity
                .created(URI.create("/api/v1/game/" + createdGame.getGameId()))
                .body(createdGame);
    }

    @PutMapping("/{game-id}/pit/{pit-id}")
    public ResponseEntity<GameResponse> moveBeans(@PathVariable("game-id") final UUID gameId,
                                                  @PathVariable("pit-id")
                                                  @Min(value = Board.PIT_START_INDEX, message = "pitId can be min : "
                                                          + Board.PIT_START_INDEX)
                                                  @Max(value = Board.PIT_END_INDEX, message = "pitId can be max : "
                                                          + Board.PIT_END_INDEX) final Integer pitId) {
        return ResponseEntity.ok(gameService.play(gameId, pitId));
    }

    @DeleteMapping("/leave/{game-id}")
    public ResponseEntity<GameResponse> leaveGame(@PathVariable("game-id") final UUID gameId) {
        return ResponseEntity.ok(gameService.leaveGame(gameId));
    }

}

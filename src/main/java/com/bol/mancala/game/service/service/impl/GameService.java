package com.bol.mancala.game.service.service.impl;

import com.bol.mancala.game.service.exception.EntityNotFoundException;
import com.bol.mancala.game.service.exception.IllegalMoveException;
import com.bol.mancala.game.service.mapper.GameResponseMapper;
import com.bol.mancala.game.service.model.Board;
import com.bol.mancala.game.service.model.Game;
import com.bol.mancala.game.service.model.Pit;
import com.bol.mancala.game.service.model.Player;
import com.bol.mancala.game.service.model.GameResponse;
import com.bol.mancala.game.service.model.NewGameRequest;
import com.bol.mancala.game.service.model.OngoingGameResponse;
import com.bol.mancala.game.service.repository.IGameRepository;
import com.bol.mancala.game.service.service.IGameService;
import lombok.RequiredArgsConstructor;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class GameService implements IGameService {

    private final IGameRepository gameRepository;
    private GameResponseMapper gameResponseMapper = Mappers.getMapper(GameResponseMapper.class);

    @Override
    public GameResponse createGame(NewGameRequest newGameRequest) {

        Optional<UUID> ongoingGame = gameRepository.findOngoingGame();
        ongoingGame.ifPresent(gameRepository::deleteById);
        Game savedGame = gameRepository.save(new Game(newGameRequest));
        return gameResponseMapper.gameToGameResponseMapper(savedGame);
    }

    @Override
    public GameResponse getGameById(UUID gameId) {
        return Optional.of(findGameById(gameId))
                .map(gameResponseMapper::gameToGameResponseMapper)
                .get();
    }

    @Override
    public OngoingGameResponse getOngoingGame() {
        OngoingGameResponse response = new OngoingGameResponse();
        gameRepository.findOngoingGame()
                .ifPresent(uuid -> {
                    response.setOngoingGameId(uuid);
                });
        return response;
    }

    @Override
    public GameResponse play(final UUID gameId, final Integer startPitId) {

        Game currentGame = findGameById(gameId);
        validateMove(currentGame, startPitId);
        int lastPitId = distribution(currentGame, startPitId);
        capturedTurn(currentGame, lastPitId);
        calculateNextPlayer(currentGame, lastPitId);
        checkIsTheGameFinished(currentGame);

        return Optional.of(currentGame)
                .map(gameResponseMapper::gameToGameResponseMapper)
                .get();
    }

    @Override
    public GameResponse leaveGame(UUID gameId) {
        Game game = findGameById(gameId);
        int totalBeanCount = 72, noBean = 0;
        Optional.ofNullable(game.getTurn())
                .ifPresentOrElse(player -> {
                            if (player.equals(Player.PLAYER_NORTH)) {
                                game.getBoard().getPit(Player.PLAYER_SOUTH.getHomePit()).setBeanCount(totalBeanCount);
                                game.getBoard().getPit(Player.PLAYER_NORTH.getHomePit()).setBeanCount(noBean);
                                game.setWinner(Player.PLAYER_SOUTH);
                            } else {
                                game.getBoard().getPit(Player.PLAYER_NORTH.getHomePit()).setBeanCount(totalBeanCount);
                                game.getBoard().getPit(Player.PLAYER_SOUTH.getHomePit()).setBeanCount(noBean);
                                game.setWinner(Player.PLAYER_NORTH);
                            }
                            game.getBoard().clearBoardAfterAPlayerWon();
                        }
                        , () -> {
                            throw new IllegalMoveException("Game has not started yet!");
                        });
        return gameResponseMapper.gameToGameResponseMapper(game);
    }

    private Game findGameById(UUID gameId) {
        return gameRepository
                .findById(gameId)
                .orElseThrow(() -> {
                    throw new EntityNotFoundException(String.format("Game Not Found gameId : %s", gameId));
                });
    }

    private void validateMove(Game currentGame, int startPitId) {

        Pit startPit = currentGame.getBoard().getPit(startPitId);

        if (startPit.isHomePit()) {
            throw new IllegalMoveException("Hop ! Can not play from home pit");
        }
        if (Player.PLAYER_NORTH.equals(currentGame.getTurn())
                && !Player.PLAYER_NORTH.equals(startPit.getOwner())) {
            throw new IllegalMoveException(String.format("Hop ! It's this turn %s will play", currentGame.getPlayerNorthName()));
        }
        if (Player.PLAYER_SOUTH.equals(currentGame.getTurn())
                && !Player.PLAYER_SOUTH.equals(startPit.getOwner())) {
            throw new IllegalMoveException(String.format("Hop ! It's this turn %s will play", currentGame.getPlayerNorthName()));
        }
        if (startPit.getBeanCount() == 0) {
            throw new IllegalMoveException("Hop ! Can not start from empty pit");
        }
        if (currentGame.getTurn() == null) {
            if (Player.PLAYER_NORTH.equals(startPit.getOwner())) {
                currentGame.setTurn(Player.PLAYER_NORTH);
            } else {
                currentGame.setTurn(Player.PLAYER_SOUTH);
            }
        }
    }

    private int distribution(final Game currentGame, final int startPitId) {
        int pitId = startPitId;
        final Pit startPit = currentGame.getBoard().getPit(startPitId);

        int beanToDistribute = startPit.getBeanCount();
        startPit.setBeanCount(0);
        while (beanToDistribute > 0) {
            final Pit currentPit = currentGame.getBoard().getPit(++pitId);
            if (!currentPit.isOpponentHomePit(currentGame.getTurn())) {
                currentPit.setBeanCount(currentPit.getBeanCount() + 1);
                beanToDistribute--;
            }
        }
        return pitId;
    }

    private void capturedTurn(Game currentGame, final int lastPitId) {
        final Pit endPit = currentGame.getBoard().getPit(lastPitId);
        if (!endPit.isHomePit() && endPit.getOwner().equals(currentGame.getTurn())
                && (endPit.getBeanCount() == 1)) {
            final Pit oppositePit = currentGame.getBoard().getPit(Board.PIT_END_INDEX - endPit.getPitId());
            if (oppositePit.getBeanCount() > 0) {
                final Pit house = currentGame.getBoard().getPit(endPit.getOwner().getHomePit());
                house.setBeanCount(
                        (house.getBeanCount() + oppositePit.getBeanCount()) + endPit.getBeanCount());
                oppositePit.setBeanCount(0);
                endPit.setBeanCount(0);
            }
        }
    }

    private void calculateNextPlayer(final Game currentGame, final int lastPitId) {

        final Pit pit = currentGame.getBoard().getPit(lastPitId);

        if (!pit.isHomePit()) {
            if (Player.PLAYER_NORTH.equals(currentGame.getTurn())) {
                currentGame.setTurn(Player.PLAYER_SOUTH);
            } else {
                currentGame.setTurn(Player.PLAYER_NORTH);
            }
        } else {
            if (Player.PLAYER_NORTH.equals(pit.getOwner())
                    && Player.PLAYER_NORTH.equals(currentGame.getTurn())) {
                currentGame.setTurn(Player.PLAYER_NORTH);
            } else if (Player.PLAYER_SOUTH.equals(pit.getOwner())
                    && Player.PLAYER_SOUTH.equals(currentGame.getTurn())) {
                currentGame.setTurn(Player.PLAYER_SOUTH);
            }
        }

    }

    private void checkIsTheGameFinished(final Game game) {
        final int playerNorthPitBeanCount = game.getBoard().getPlayerSideTotalBeanCount(Player.PLAYER_NORTH, false);
        final int playerSouthPitBeanCount = game.getBoard().getPlayerSideTotalBeanCount(Player.PLAYER_SOUTH, false);
        if ((playerNorthPitBeanCount == 0) || (playerSouthPitBeanCount == 0)) {
            final Pit houseNorth = game.getBoard().getPit(Player.PLAYER_NORTH.getHomePit());
            final Pit houseSouth = game.getBoard().getPit(Player.PLAYER_SOUTH.getHomePit());
            houseNorth.setBeanCount(houseNorth.getBeanCount() + playerNorthPitBeanCount);
            houseSouth.setBeanCount(houseSouth.getBeanCount() + playerSouthPitBeanCount);
            game.getBoard().clearBoardAfterAPlayerWon();
            determineWinner(game);
        }
    }

    private void determineWinner(final Game game) {
        final int houseNorthBeanCount = game.getBoard().getPlayerSideTotalBeanCount(Player.PLAYER_NORTH, true);
        final int houseSouthBeanCount = game.getBoard().getPlayerSideTotalBeanCount(Player.PLAYER_SOUTH, true);
        if (houseNorthBeanCount > houseSouthBeanCount) {
            game.setWinner(Player.PLAYER_NORTH);
        } else if (houseNorthBeanCount < houseSouthBeanCount) {
            game.setWinner(Player.PLAYER_SOUTH);
        }
    }
}

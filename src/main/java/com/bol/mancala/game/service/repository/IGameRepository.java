package com.bol.mancala.game.service.repository;

import com.bol.mancala.game.service.model.Game;

import java.util.Optional;
import java.util.UUID;

public interface IGameRepository {
    Game save(final Game game);
    Optional<Game> findById(final UUID uuid);
    void deleteById(final UUID uuid);
    Optional<UUID> findOngoingGame();
}

package com.bol.mancala.game.service.repository.impl;

import com.bol.mancala.game.service.model.Game;
import com.bol.mancala.game.service.repository.IGameRepository;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@Repository
public class GameRepository implements IGameRepository {
    private final Map<UUID, Game> repository = new HashMap<>();

    @Override
    public Game save(Game game) {
        this.repository.put(game.getGameId(), game);
        return this.repository.get(game.getGameId());
    }

    @Override
    public Optional<Game> findById(UUID uuid) {
        return Optional.ofNullable(repository.get(uuid));
    }

    @Override
    public void deleteById(UUID uuid) {
        repository.remove(uuid);
    }

    @Override
    public Optional<UUID> findOngoingGame() {
        Optional<UUID> ongoingGame = repository.values().stream()
                .filter(game -> game.getWinner() == null)
                .map(game -> game.getGameId())
                .findAny();
        return ongoingGame;
    }
}

package com.bol.mancala.game.service.model;

import lombok.Data;

import java.util.UUID;

@Data
public class Game {

    public static final int BEAN_COUNT = 6;

    private final UUID gameId;
    private final Board board;
    private String playerNorthName;
    private String playerSouthName;
    private Player turn;
    private Player winner;

    public Game(NewGameRequest newGameRequest){
        this.gameId = UUID.randomUUID();
        this.board = new Board();
        this.playerNorthName = newGameRequest.getPlayerNorthName();
        this.playerSouthName = newGameRequest.getPlayerSouthName();
    }

    public Game(NewGameRequest newGameRequest,Board board){
        this.gameId = UUID.randomUUID();
        this.board = board;
        this.playerNorthName = newGameRequest.getPlayerNorthName();
        this.playerSouthName = newGameRequest.getPlayerSouthName();
    }
}

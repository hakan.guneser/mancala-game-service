package com.bol.mancala.game.service.mapper;

import com.bol.mancala.game.service.model.Game;
import com.bol.mancala.game.service.model.Pit;
import com.bol.mancala.game.service.model.GameResponse;
import org.mapstruct.*;

import java.util.stream.Collectors;

@Mapper
public interface GameResponseMapper {

    @Mappings({
            @Mapping(target = "gameId", source = "game.gameId"),
            @Mapping(target = "playerNorthName", source = "game.playerNorthName"),
            @Mapping(target = "playerSouthName", source = "game.playerSouthName"),
            @Mapping(target = "turn", source = "game.turn"),
            @Mapping(target = "winner", source = "game.winner")
    })
    GameResponse gameToGameResponseMapper(Game game);

    @AfterMapping
    default void finishGameResponseMapper(@MappingTarget GameResponse gameResponse, Game game) {
        gameResponse.setBoardStatus(game
                .getBoard()
                .getPits()
                .stream()
                .collect(Collectors.toMap(Pit::getPitId, Pit::getBeanCount)));
    }

}

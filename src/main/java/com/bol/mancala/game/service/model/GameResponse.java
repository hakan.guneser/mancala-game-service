package com.bol.mancala.game.service.model;

import com.bol.mancala.game.service.model.Player;
import lombok.Data;

import java.util.Map;
import java.util.UUID;

@Data
public class GameResponse {
    private UUID gameId;
    private String playerNorthName;
    private String playerSouthName;
    private Player turn;
    private Player winner;
    private Map<Integer, Integer> boardStatus;
}

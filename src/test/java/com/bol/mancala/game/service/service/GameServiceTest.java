package com.bol.mancala.game.service.service;

import com.bol.mancala.game.service.exception.EntityNotFoundException;
import com.bol.mancala.game.service.exception.IllegalMoveException;
import com.bol.mancala.game.service.mapper.GameResponseMapper;
import com.bol.mancala.game.service.model.Board;
import com.bol.mancala.game.service.model.Game;
import com.bol.mancala.game.service.model.Pit;
import com.bol.mancala.game.service.model.Player;
import com.bol.mancala.game.service.model.GameResponse;
import com.bol.mancala.game.service.model.NewGameRequest;
import com.bol.mancala.game.service.model.OngoingGameResponse;
import com.bol.mancala.game.service.repository.IGameRepository;
import com.bol.mancala.game.service.service.impl.GameService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;


class GameServiceTest {

    private GameService underTest;
    private GameResponseMapper gameResponseMapper = Mappers.getMapper(GameResponseMapper.class);

    //Given player names
    private final String playerNorthName = "John";
    private final String playerSouthName = "Mehmet";
    private NewGameRequest newGameRequest;

    @Mock
    private IGameRepository gameRepository;

    @Captor
    private ArgumentCaptor<Game> gameArgumentCaptor;

    @Captor
    private ArgumentCaptor<UUID> gameIdArgumentCaptor;

    @BeforeEach
    void setUp() {
        newGameRequest =
                NewGameRequest.builder()
                        .playerNorthName(playerNorthName)
                        .playerSouthName(playerSouthName)
                        .build();

        MockitoAnnotations.openMocks(this);
        underTest = new GameService(gameRepository);
    }

    @Test
    void itShouldCreateGameWhenHasNoOngoingGame() {

        //Given expected Game Response
        Game expectedResponse = new Game(newGameRequest);
        //... has any ongoing game
        given(gameRepository.findOngoingGame()).willReturn(Optional.empty());
        // When
        underTest.createGame(newGameRequest);

        //Then
        then(gameRepository).should().save(gameArgumentCaptor.capture());
        Game gameArgumentCaptorValue = gameArgumentCaptor.getValue();

        assertThat(gameArgumentCaptorValue)
                .isNotNull()
                .usingRecursiveComparison()
                .ignoringFields("gameId")
                .isEqualTo(expectedResponse);

    }

    @Test
    void itShouldDeleteOngoingAndCreateNewGame() {

        //Given a gameID
        final UUID ongoingGameId = UUID.randomUUID();
        // ... has any ongoing game
        given(gameRepository.findOngoingGame()).willReturn(Optional.of(ongoingGameId));
        // ... expected Game Response
        Game expectedResponse = new Game(newGameRequest);

        //When
        underTest.createGame(newGameRequest);
        //Then
        then(gameRepository).should().deleteById(gameIdArgumentCaptor.capture());
        UUID gameIdArgumentCaptorValue = gameIdArgumentCaptor.getValue();

        assertThat(gameIdArgumentCaptorValue)
                .isNotNull()
                .isEqualTo(ongoingGameId);

        then(gameRepository).should().save(gameArgumentCaptor.capture());
        Game gameArgumentCaptorValue = gameArgumentCaptor.getValue();

        assertThat(gameArgumentCaptorValue)
                .isNotNull()
                .usingRecursiveComparison()
                .ignoringFields("gameId")
                .isEqualTo(expectedResponse);
    }

    @Test
    void itShouldGetGameByIdWhenGameExists() {

        //Given expected Game Object
        Game expectedGame = new Game(newGameRequest);
        //... expected GameResponse Object
        GameResponse expectedGameResponse = gameResponseMapper.gameToGameResponseMapper(expectedGame);
        //... gameId of expected game response
        UUID gameId = expectedGameResponse.getGameId();
        //... has any ongoing game
        given(gameRepository.findById(gameId)).willReturn(Optional.of(expectedGame));

        //When
        GameResponse actualGameById = underTest.getGameById(gameId);
        //Then
        assertThat(actualGameById)
                .isNotNull()
                .usingRecursiveComparison()
                .isEqualTo(expectedGameResponse);
    }

    @Test
    void itShouldRaiseErrorWhenGameNotExists() {

        //Given request gameId
        UUID gameId = UUID.randomUUID();
        //... has any ongoing game
        given(gameRepository.findById(gameId)).willReturn(Optional.ofNullable(null));

        //When
        //Then
        assertThatThrownBy(() -> {
            underTest.getGameById(gameId);
        })
                .isInstanceOf(EntityNotFoundException.class)
                .hasMessageContaining(String.format("Game Not Found gameId : %s", gameId));
    }

    @Test
    void itShouldReturnGameIdWhenHasOngoingGame() {
        // given request gameId
        UUID gameId = UUID.randomUUID();
        // ... expected game response
        OngoingGameResponse expectedResponse = new OngoingGameResponse(gameId);
        // ... has any ongoing game
        given(gameRepository.findOngoingGame()).willReturn(Optional.ofNullable(gameId));
        //When
        OngoingGameResponse ongoingGame = underTest.getOngoingGame();
        //Then
        assertThat(ongoingGame)
                .isNotNull()
                .usingRecursiveComparison()
                .isEqualTo(expectedResponse);
    }

    @Test
    void itShouldReturnNullAsIdWhenHasNoOngoingGame() {
        //Given has any ongoing game
        given(gameRepository.findOngoingGame()).willReturn(Optional.ofNullable(null));
        //... expected game response
        OngoingGameResponse expectedResponse = new OngoingGameResponse();
        //When
        OngoingGameResponse ongoingGame = underTest.getOngoingGame();
        //Then
        assertThat(ongoingGame)
                .isNotNull()
                .usingRecursiveComparison()
                .isEqualTo(expectedResponse);
    }

    @Test
    void itShouldNotMoveBeanIfStartPitIsHomePit() {
        //Given game id
        final UUID gameId = UUID.randomUUID();
        // ... a fresh game
        Game game = new Game(newGameRequest);
        given(gameRepository.findById(gameId)).willReturn(Optional.of(game));

        //When
        //Then
        assertThatThrownBy(() -> {
            underTest.play(gameId, Player.PLAYER_NORTH.getHomePit());
        })
                .hasMessageContaining("Can not play from home pit")
                .isInstanceOf(IllegalMoveException.class);
    }

    @Test
    void itShouldNotMoveBeanIfNotCorrectPlayer_vSouth() {
        //Given game id
        final UUID gameId = UUID.randomUUID();
        //... a new game and set turn as Player south
        Game game = new Game(newGameRequest);
        game.setTurn(Player.PLAYER_SOUTH);
        given(gameRepository.findById(gameId)).willReturn(Optional.of(game));

        //When
        //Then
        assertThatThrownBy(() -> {
            underTest.play(gameId, 3);
        })
                .hasMessageContaining(String.format("Hop ! It's this turn %s will play", playerNorthName))
                .isInstanceOf(IllegalMoveException.class);
    }

    @Test
    void itShouldNotMoveBeanIfNotCorrectPlayer_vNorth() {
        //Given game id
        final UUID gameId = UUID.randomUUID();
        // ... a new game and set turn as Player north
        Game game = new Game(newGameRequest);
        game.setTurn(Player.PLAYER_NORTH);
        given(gameRepository.findById(gameId)).willReturn(Optional.of(game));

        //When
        //Then
        assertThatThrownBy(() -> {
            underTest.play(gameId, 9);
        })
                .hasMessageContaining(String.format("Hop ! It's this turn %s will play", playerNorthName))
                .isInstanceOf(IllegalMoveException.class);
    }

    @Test
    void itShouldNotMoveBeanIfStartPitIsEmpty() {
        //Given game id
        final UUID gameId = UUID.randomUUID();
        // ... an ongoing game board
        List<Pit> pits = Arrays.asList(
                new Pit(1, 5),
                new Pit(2, 5),
                new Pit(3, 5),
                new Pit(4, 5),
                new Pit(5, 5),
                new Pit(6, 5),
                new Pit(7, 5),
                new Pit(8, 5),
                new Pit(9, 0),
                new Pit(10, 5),
                new Pit(11, 5),
                new Pit(12, 5),
                new Pit(13, 5),
                new Pit(14, 5));
        Board board = new Board(pits);
        // ... a ongoing game
        Game game = new Game(newGameRequest, board);

        given(gameRepository.findById(gameId)).willReturn(Optional.of(game));

        //When
        //Then
        assertThatThrownBy(() -> {
            underTest.play(gameId, 9);
        })
                .hasMessageContaining(String.format("Hop ! Can not start from empty pit", playerNorthName))
                .isInstanceOf(IllegalMoveException.class);
    }

    @Test
    void itShouldReturnGameWithTurnPlayerSouth() {
        //Given a game id
        UUID gameId = UUID.randomUUID();
        //... a new game and set turn for player north
        Game gameBeforeMove = new Game(newGameRequest);
        gameBeforeMove.setTurn(Player.PLAYER_NORTH);

        given(gameRepository.findById(gameId)).willReturn(Optional.of(gameBeforeMove));

        //When
        GameResponse gameAfterMove = underTest.play(gameId, 2);
        //Then
        assertThat(gameAfterMove.getTurn()).isEqualTo(Player.PLAYER_SOUTH);
    }

    @Test
    void itShouldReturnGameWithTurnPlayerNorth() {
        // given a game id
        UUID gameId = UUID.randomUUID();
        //... a new game and set turn for player south
        Game gameBeforeMove = new Game(newGameRequest);
        gameBeforeMove.setTurn(Player.PLAYER_SOUTH);

        given(gameRepository.findById(gameId)).willReturn(Optional.of(gameBeforeMove));

        //When
        GameResponse gameAfterMove = underTest.play(gameId, 9);
        //Then
        assertThat(gameAfterMove.getTurn()).isEqualTo(Player.PLAYER_NORTH);
    }

    @Test
    void itShouldWinExtraTurnWhenNorthLastPitIsHomePit() {
        //Given game id
        final UUID gameId = UUID.randomUUID();
        //... a game
        Game gameBeforeMove = new Game(newGameRequest);
        gameBeforeMove.setTurn(Player.PLAYER_NORTH);

        given(gameRepository.findById(gameId)).willReturn(Optional.of(gameBeforeMove));
        //When
        GameResponse gameAfterMove = underTest.play(gameId, 1);
        //Then
        assertThat(gameAfterMove.getTurn()).isEqualTo(Player.PLAYER_NORTH);
    }

    @Test
    void itShouldWinExtraTurnWhenSouthLastPitIsHomePit() {
        //Given game id
        final UUID gameId = UUID.randomUUID();
        //... a game
        Game gameBeforeMove = new Game(newGameRequest);
        gameBeforeMove.setTurn(Player.PLAYER_SOUTH);

        given(gameRepository.findById(gameId)).willReturn(Optional.of(gameBeforeMove));
        //When
        GameResponse gameAfterMove = underTest.play(gameId, 8);
        //Then
        assertThat(gameAfterMove.getTurn()).isEqualTo(Player.PLAYER_SOUTH);
    }

    @Test
    void itShouldCapturedIfLastBeanMovedToEmptyPit() {
        //Given game id
        final UUID gameId = UUID.randomUUID();
        // ... an ongoing game board before move
        List<Pit> pitsBeforeMove = Arrays.asList(
                new Pit(1, 5),
                new Pit(2, 1),
                new Pit(3, 0),
                new Pit(4, 5),
                new Pit(5, 5),
                new Pit(6, 5),
                new Pit(7, 5),
                new Pit(8, 5),
                new Pit(9, 0),
                new Pit(10, 5),
                new Pit(11, 5),
                new Pit(12, 5),
                new Pit(13, 5),
                new Pit(14, 5));
        Board boardBeforeMove = new Board(pitsBeforeMove);
        Game gameBeforeMove = new Game(newGameRequest, boardBeforeMove);
        gameBeforeMove.setTurn(Player.PLAYER_NORTH);
        // ... an ongoing game board before move
        List<Pit> expectedPitsAfterMove = Arrays.asList(
                new Pit(1, 5),
                new Pit(2, 0),
                new Pit(3, 0),
                new Pit(4, 5),
                new Pit(5, 5),
                new Pit(6, 5),
                new Pit(7, 11),
                new Pit(8, 5),
                new Pit(9, 0),
                new Pit(10, 5),
                new Pit(11, 0),
                new Pit(12, 5),
                new Pit(13, 5),
                new Pit(14, 5));
        Board expectedBoardAfterMove = new Board(expectedPitsAfterMove);
        Game expectedGameAfterMove = new Game(newGameRequest, expectedBoardAfterMove);
        expectedGameAfterMove.setTurn(Player.PLAYER_SOUTH);
        GameResponse expectedGameResponseAfterMove = gameResponseMapper.gameToGameResponseMapper(expectedGameAfterMove);

        given(gameRepository.findById(gameId)).willReturn(Optional.of(gameBeforeMove));

        //When
        GameResponse gameAfterMove = underTest.play(gameId, 2);
        //Then
        assertThat(gameAfterMove)
                .isNotNull()
                .usingRecursiveComparison()
                .ignoringFields("gameId")
                .isEqualTo(expectedGameResponseAfterMove);
    }

    @Test
    void itShouldFinishIfPlayerSouthAllBeansFinishOnASide() {
        //Given game id
        final UUID gameId = UUID.randomUUID();
        // ... an ongoing game board before move
        List<Pit> pitsBeforeMove = Arrays.asList(
                new Pit(1, 0),
                new Pit(2, 0),
                new Pit(3, 0),
                new Pit(4, 0),
                new Pit(5, 0),
                new Pit(6, 5),
                new Pit(7, 12),
                new Pit(8, 5),
                new Pit(9, 0),
                new Pit(10, 5),
                new Pit(11, 5),
                new Pit(12, 5),
                new Pit(13, 5),
                new Pit(14, 5));
        Board boardBeforeMove = new Board(pitsBeforeMove);
        Game gameBeforeMove = new Game(newGameRequest, boardBeforeMove);
        gameBeforeMove.setTurn(Player.PLAYER_NORTH);
        // ... an ongoing game board before move
        List<Pit> expectedPitsAfterMove = Arrays.asList(
                new Pit(1, 0),
                new Pit(2, 0),
                new Pit(3, 0),
                new Pit(4, 0),
                new Pit(5, 0),
                new Pit(6, 0),
                new Pit(7, 13),
                new Pit(8, 0),
                new Pit(9, 0),
                new Pit(10, 0),
                new Pit(11, 0),
                new Pit(12, 0),
                new Pit(13, 0),
                new Pit(14, 34));
        Board expectedBoardAfterMove = new Board(expectedPitsAfterMove);
        Game expectedGameAfterMove = new Game(newGameRequest, expectedBoardAfterMove);
        expectedGameAfterMove.setTurn(Player.PLAYER_SOUTH);
        expectedGameAfterMove.setWinner(Player.PLAYER_SOUTH);
        GameResponse expectedGameResponseAfterMove = gameResponseMapper.gameToGameResponseMapper(expectedGameAfterMove);

        given(gameRepository.findById(gameId)).willReturn(Optional.of(gameBeforeMove));

        //When
        GameResponse gameAfterMove = underTest.play(gameId, 6);
        //Then
        assertThat(gameAfterMove)
                .isNotNull()
                .usingRecursiveComparison()
                .ignoringFields("gameId")
                .isEqualTo(expectedGameResponseAfterMove);
    }

    @Test
    void itShouldFinishIfPlayerNorthAllBeansFinishOnASide() {
        //Given game id
        final UUID gameId = UUID.randomUUID();
        // ... an ongoing game board before move
        List<Pit> pitsBeforeMove = Arrays.asList(
                new Pit(1, 0),
                new Pit(2, 0),
                new Pit(3, 0),
                new Pit(4, 0),
                new Pit(5, 0),
                new Pit(6, 5),
                new Pit(7, 45),
                new Pit(8, 5),
                new Pit(9, 0),
                new Pit(10, 5),
                new Pit(11, 5),
                new Pit(12, 5),
                new Pit(13, 5),
                new Pit(14, 5));
        Board boardBeforeMove = new Board(pitsBeforeMove);
        Game gameBeforeMove = new Game(newGameRequest, boardBeforeMove);
        gameBeforeMove.setTurn(Player.PLAYER_NORTH);
        // ... an ongoing game board before move
        List<Pit> expectedPitsAfterMove = Arrays.asList(
                new Pit(1, 0),
                new Pit(2, 0),
                new Pit(3, 0),
                new Pit(4, 0),
                new Pit(5, 0),
                new Pit(6, 0),
                new Pit(7, 46),
                new Pit(8, 0),
                new Pit(9, 0),
                new Pit(10, 0),
                new Pit(11, 0),
                new Pit(12, 0),
                new Pit(13, 0),
                new Pit(14, 34));
        Board expectedBoardAfterMove = new Board(expectedPitsAfterMove);
        Game expectedGameAfterMove = new Game(newGameRequest, expectedBoardAfterMove);
        expectedGameAfterMove.setTurn(Player.PLAYER_SOUTH);
        expectedGameAfterMove.setWinner(Player.PLAYER_NORTH);
        GameResponse expectedGameResponseAfterMove = gameResponseMapper.gameToGameResponseMapper(expectedGameAfterMove);

        given(gameRepository.findById(gameId)).willReturn(Optional.of(gameBeforeMove));

        //When
        GameResponse gameAfterMove = underTest.play(gameId, 6);
        //Then
        assertThat(gameAfterMove)
                .isNotNull()
                .usingRecursiveComparison()
                .ignoringFields("gameId")
                .isEqualTo(expectedGameResponseAfterMove);
    }

    @Test
    void itShouldNotPutBeanOpponentHome() {
        //Given game id
        final UUID gameId = UUID.randomUUID();
        // ... an ongoing game board before move
        List<Pit> pitsBeforeMove = Arrays.asList(
                new Pit(1, 5),
                new Pit(2, 5),
                new Pit(3, 5),
                new Pit(4, 5),
                new Pit(5, 5),
                new Pit(6, 9),
                new Pit(7, 5),
                new Pit(8, 5),
                new Pit(9, 5),
                new Pit(10, 5),
                new Pit(11, 5),
                new Pit(12, 5),
                new Pit(13, 5),
                new Pit(14, 5));
        Board boardBeforeMove = new Board(pitsBeforeMove);
        Game gameBeforeMove = new Game(newGameRequest, boardBeforeMove);
        gameBeforeMove.setTurn(Player.PLAYER_NORTH);
        // ... an ongoing game board before move
        List<Pit> expectedPitsAfterMove = Arrays.asList(
                new Pit(1, 6),
                new Pit(2, 6),
                new Pit(3, 5),
                new Pit(4, 5),
                new Pit(5, 5),
                new Pit(6, 0),
                new Pit(7, 6),
                new Pit(8, 6),
                new Pit(9, 6),
                new Pit(10, 6),
                new Pit(11, 6),
                new Pit(12, 6),
                new Pit(13, 6),
                new Pit(14, 5));
        Board expectedBoardAfterMove = new Board(expectedPitsAfterMove);
        Game expectedGameAfterMove = new Game(newGameRequest, expectedBoardAfterMove);
        expectedGameAfterMove.setTurn(Player.PLAYER_SOUTH);
        GameResponse expectedGameResponseAfterMove = gameResponseMapper.gameToGameResponseMapper(expectedGameAfterMove);

        given(gameRepository.findById(gameId)).willReturn(Optional.of(gameBeforeMove));

        //When
        GameResponse gameAfterMove = underTest.play(gameId, 6);
        //Then
        assertThat(gameAfterMove)
                .isNotNull()
                .usingRecursiveComparison()
                .ignoringFields("gameId")
                .isEqualTo(expectedGameResponseAfterMove);
    }

    @Test
    void itShouldTurnNorthIfSouthStarted() {
        //Given game id
        final UUID gameId = UUID.randomUUID();
        Game gameBeforeMove = new Game(newGameRequest);
        // ... an ongoing game board before move
        given(gameRepository.findById(gameId)).willReturn(Optional.of(gameBeforeMove));

        //When
        GameResponse gameAfterMove = underTest.play(gameId, 9);
        //Then
        assertThat(gameAfterMove.getTurn())
                .isNotNull()
                .isEqualTo(Player.PLAYER_NORTH);
    }

    @Test
    void itShouldTurnSouthIfNorthStarted() {
        //Given game id
        final UUID gameId = UUID.randomUUID();
        Game gameBeforeMove = new Game(newGameRequest);
        // ... an ongoing game board before move
        given(gameRepository.findById(gameId)).willReturn(Optional.of(gameBeforeMove));

        //When
        GameResponse gameAfterMove = underTest.play(gameId, 2);
        //Then
        assertThat(gameAfterMove.getTurn())
                .isNotNull()
                .isEqualTo(Player.PLAYER_SOUTH);
    }

    @Test
    void itShouldNotLeaveGameIfGameNotExists() {
        //Given game id
        final UUID gameId = UUID.randomUUID();
        // ... an ongoing game board before move
        given(gameRepository.findById(gameId)).willReturn(Optional.ofNullable(null));

        //When
        //Then
        assertThatThrownBy(() -> {
            underTest.leaveGame(gameId);
        })
                .hasMessageContaining(String.format("Game Not Found gameId : %s",gameId))
                .isInstanceOf(EntityNotFoundException.class);
    }

    @Test
    void itShouldNotLeaveGameIfGameHasNotStartedYet() {
        //Given game id
        final UUID gameId = UUID.randomUUID();
        Game game = new Game(newGameRequest);
        // ... an ongoing game board before move
        given(gameRepository.findById(gameId)).willReturn(Optional.ofNullable(game));

        //When
        //Then
        assertThatThrownBy(() -> {
            underTest.leaveGame(gameId);
        })
                .hasMessageContaining("Game has not started yet!")
                .isInstanceOf(IllegalMoveException.class);
    }

    @Test
    void itShouldPlayerNorthWinWhenPlayerSouthLeaveGame() {
        //Given game id
        final UUID gameId = UUID.randomUUID();
        Game game = new Game(newGameRequest);
        game.setTurn(Player.PLAYER_SOUTH);
        // ... an ongoing game board before move
        given(gameRepository.findById(gameId)).willReturn(Optional.ofNullable(game));


        //When
        GameResponse gameResponse = underTest.leaveGame(gameId);
        //Then
        assertThat(gameResponse.getWinner()).isEqualTo(Player.PLAYER_NORTH);
    }
    @Test
    void itShouldPlayerSouthWinWhenPlayerNorthLeaveGame() {
        //Given game id
        final UUID gameId = UUID.randomUUID();
        //... new game
        Game game = new Game(newGameRequest);
        game.setTurn(Player.PLAYER_NORTH);
        //... an ongoing game board before move
        given(gameRepository.findById(gameId)).willReturn(Optional.ofNullable(game));

        //When
        GameResponse gameResponse = underTest.leaveGame(gameId);
        //Then
        assertThat(gameResponse.getWinner()).isEqualTo(Player.PLAYER_SOUTH);
    }
}
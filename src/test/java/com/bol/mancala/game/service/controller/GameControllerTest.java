package com.bol.mancala.game.service.controller;

import com.bol.mancala.game.service.model.Player;
import com.bol.mancala.game.service.model.GameResponse;
import com.bol.mancala.game.service.model.NewGameRequest;
import com.bol.mancala.game.service.service.impl.GameService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Objects;

import static org.junit.jupiter.api.Assertions.fail;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class GameControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private GameService gameService;

    private NewGameRequest newGameRequest;
    private String playerNorthName = "Hakan";
    private String playerSouthName = "John";

    @BeforeEach
    void initialize(){
        newGameRequest = NewGameRequest.builder()
                .playerNorthName(playerNorthName)
                .playerSouthName(playerSouthName)
                .build();
    }

    @Test
    void itShouldCreateNewGame() throws Exception {

        //When
        ResultActions gameCreateAction = mockMvc.perform(post("/api/v1/game/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content(Objects.requireNonNull(objectToJson(newGameRequest))));
        //Then
        gameCreateAction
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.playerNorthName").value(playerNorthName))
                .andExpect(MockMvcResultMatchers.jsonPath("$.playerSouthName").value(playerSouthName))
                .andExpect(MockMvcResultMatchers.jsonPath("$.boardStatus").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.winner").isEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.turn").isEmpty());
    }

    @Test
    void itShouldGetGameId() throws Exception {

        // given new game by service
        GameResponse game = gameService.createGame(newGameRequest);
        //When
        ResultActions gameCreateAction = mockMvc.perform(get("/api/v1/game/" + game.getGameId()));
        //Then
        gameCreateAction
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.playerNorthName").value(playerNorthName))
                .andExpect(MockMvcResultMatchers.jsonPath("$.playerSouthName").value(playerSouthName))
                .andExpect(MockMvcResultMatchers.jsonPath("$.boardStatus").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.winner").isEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.turn").isEmpty());
    }

    @Test
    void itShouldGetOngoingGameId() throws Exception {
        //Given new game by service
        GameResponse game = gameService.createGame(newGameRequest);
        //When
        ResultActions gameCreateAction = mockMvc.perform(get("/api/v1/game/ongoing"));
        //Then
        gameCreateAction
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.ongoingGameId").value(game.getGameId().toString()));
    }

    @Test
    void itShouldMoveBeans() throws Exception {
        //Given new game by service
        GameResponse game = gameService.createGame(newGameRequest);
        //When
        ResultActions gameCreateAction = mockMvc.perform(put(String.format("/api/v1/game/%s/pit/5", game.getGameId())));
        //Then
        gameCreateAction
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gameId").value(game.getGameId().toString()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.turn").value(Player.PLAYER_SOUTH.toString()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.boardStatus.1").value(6))
                .andExpect(MockMvcResultMatchers.jsonPath("$.boardStatus.2").value(6))
                .andExpect(MockMvcResultMatchers.jsonPath("$.boardStatus.3").value(6))
                .andExpect(MockMvcResultMatchers.jsonPath("$.boardStatus.4").value(6))
                .andExpect(MockMvcResultMatchers.jsonPath("$.boardStatus.5").value(0))
                .andExpect(MockMvcResultMatchers.jsonPath("$.boardStatus.6").value(7))
                .andExpect(MockMvcResultMatchers.jsonPath("$.boardStatus.7").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.boardStatus.8").value(7))
                .andExpect(MockMvcResultMatchers.jsonPath("$.boardStatus.9").value(7))
                .andExpect(MockMvcResultMatchers.jsonPath("$.boardStatus.10").value(7))
                .andExpect(MockMvcResultMatchers.jsonPath("$.boardStatus.11").value(7))
                .andExpect(MockMvcResultMatchers.jsonPath("$.boardStatus.12").value(6))
                .andExpect(MockMvcResultMatchers.jsonPath("$.boardStatus.13").value(6))
                .andExpect(MockMvcResultMatchers.jsonPath("$.boardStatus.14").value(0));

    }

    @Test
    void itShouldLeaveGame() throws Exception {
        //Given new game by service
        GameResponse game = gameService.createGame(newGameRequest);
        gameService.play(game.getGameId(), 2);
        //When
        ResultActions gameCreateAction = mockMvc.perform(delete(String.format("/api/v1/game/leave/%s", game.getGameId())));
        //Then
        gameCreateAction
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.gameId").value(game.getGameId().toString()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.turn").value(Player.PLAYER_SOUTH.toString()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.winner").value(Player.PLAYER_NORTH.toString()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.boardStatus.1").value(0))
                .andExpect(MockMvcResultMatchers.jsonPath("$.boardStatus.2").value(0))
                .andExpect(MockMvcResultMatchers.jsonPath("$.boardStatus.3").value(0))
                .andExpect(MockMvcResultMatchers.jsonPath("$.boardStatus.4").value(0))
                .andExpect(MockMvcResultMatchers.jsonPath("$.boardStatus.5").value(0))
                .andExpect(MockMvcResultMatchers.jsonPath("$.boardStatus.6").value(0))
                .andExpect(MockMvcResultMatchers.jsonPath("$.boardStatus.7").value(72))
                .andExpect(MockMvcResultMatchers.jsonPath("$.boardStatus.8").value(0))
                .andExpect(MockMvcResultMatchers.jsonPath("$.boardStatus.9").value(0))
                .andExpect(MockMvcResultMatchers.jsonPath("$.boardStatus.10").value(0))
                .andExpect(MockMvcResultMatchers.jsonPath("$.boardStatus.11").value(0))
                .andExpect(MockMvcResultMatchers.jsonPath("$.boardStatus.12").value(0))
                .andExpect(MockMvcResultMatchers.jsonPath("$.boardStatus.13").value(0))
                .andExpect(MockMvcResultMatchers.jsonPath("$.boardStatus.14").value(0));
    }

    private String objectToJson(Object object) {
        try {
            return new ObjectMapper().writeValueAsString(object);
        } catch (JsonProcessingException e) {
            fail("Failed to convert Object to JSON");
            return null;
        }
    }
}
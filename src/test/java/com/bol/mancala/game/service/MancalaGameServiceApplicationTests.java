package com.bol.mancala.game.service;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class MancalaGameServiceApplicationTests {

	@Test
	public void main() {
		MancalaGameServiceApplication.main(new String[] {});
	}

}

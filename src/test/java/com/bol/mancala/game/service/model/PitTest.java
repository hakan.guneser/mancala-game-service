package com.bol.mancala.game.service.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PitTest {

    @Test
    void itShouldReturnTrueIfItsHomePit() {
        final Pit pit7 = new Pit(7);
        final Pit pit14 = new Pit(14);

        assertTrue(pit7.isHomePit());
        assertTrue(pit14.isHomePit());
    }
    @Test
    void itShouldReturnFalseIfItsNotHomePit() {
        final Pit pit3 = new Pit(3);
        final Pit pit8 = new Pit(8);

        assertFalse(pit3.isHomePit());
        assertFalse(pit8.isHomePit());
    }
}
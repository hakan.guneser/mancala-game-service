package com.bol.mancala.game.service.repository;

import com.bol.mancala.game.service.model.Game;
import com.bol.mancala.game.service.model.NewGameRequest;
import com.bol.mancala.game.service.repository.impl.GameRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

class GameRepositoryTest {

    private GameRepository underTest;

    @BeforeEach
    void setUp() {
        underTest = new GameRepository();
    }

    @Test
    void itShouldSave() {
        //Given players info
        NewGameRequest newGameInput =
                NewGameRequest.builder()
                        .playerSouthName("John")
                        .playerNorthName("Mehmet")
                        .build();
        // ... a request
        Game gameToBeSave = new Game(newGameInput);

        //When
        Game savedGame = underTest.save(gameToBeSave);
        //Then
        assertThat(gameToBeSave)
                .isNotNull()
                .usingRecursiveComparison().isEqualTo(savedGame);
    }

    @Test
    void itShouldFindAndReturnGameWhenGameExists() {
        //Given players info
        NewGameRequest newGameInput =
                NewGameRequest.builder()
                        .playerSouthName("John")
                        .playerNorthName("Mehmet")
                        .build();
        // ... a request
        Game gameToBeSave = new Game(newGameInput);
        // ... a gameId
        UUID gameId = gameToBeSave.getGameId();

        //When
        Game savedGame = underTest.save(gameToBeSave);
        Optional<Game> foundGameById = underTest.findById(gameId);
        //Then
        assertThat(foundGameById.get())
                .isNotNull()
                .usingRecursiveComparison().isEqualTo(savedGame);
    }

    @Test
    void itShouldReturnNullWhenGameNotExists() {
        //Given a gameId
        UUID gameId = UUID.randomUUID();
        //When
        Optional<Game> foundGameById = underTest.findById(gameId);
        //Then
        assertThat(foundGameById)
                .isEmpty();
    }

    @Test
    void itShouldDeleteWhenGameExists() {
        //Given players info
        NewGameRequest newGameInput =
                NewGameRequest.builder()
                        .playerSouthName("John")
                        .playerNorthName("Mehmet")
                        .build();
        // ... a request
        Game gameToBeSave = new Game(newGameInput);
        // ... a gameId
        UUID gameId = gameToBeSave.getGameId();

        //When
        Game savedGame = underTest.save(gameToBeSave);
        Optional<Game> foundGameByIdBeforeDelete = underTest.findById(gameId);
        //Then
        assertThat(foundGameByIdBeforeDelete.get())
                .isNotNull()
                .usingRecursiveComparison().isEqualTo(savedGame);

        //When
        underTest.deleteById(gameId);
        //Then
        Optional<Game> foundGameByIdAfterDelete = underTest.findById(gameId);

        assertThat(foundGameByIdAfterDelete)
                .isEmpty();

    }

    @Test
    void itShouldNotDeleteWhenGameNotExists() {
        //Given  a gameId
        UUID gameId = UUID.randomUUID();
        //When
        underTest.deleteById(gameId);
        //Then
        Optional<Game> foundGameByIdAfterDelete = underTest.findById(gameId);

        assertThat(foundGameByIdAfterDelete)
                .isEmpty();

    }
    @Test
    void itShouldFindOngoingGame() {
        //Given players info
        NewGameRequest newGameInput =
                NewGameRequest.builder()
                        .playerSouthName("John")
                        .playerNorthName("Mehmet")
                        .build();
        // ... a request
        Game gameToBeSave = new Game(newGameInput);
        // ... a gameId
        UUID gameId = gameToBeSave.getGameId();

        //When
        Game savedGame = underTest.save(gameToBeSave);
        Optional<UUID> foundOngoingGameId = underTest.findOngoingGame();
        //Then
        assertThat(foundOngoingGameId.get())
                .isNotNull()
                .usingRecursiveComparison()
                .isEqualTo(gameId);

    }
}
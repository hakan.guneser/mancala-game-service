package com.bol.mancala.game.service.exception;

import com.bol.mancala.game.service.model.Board;
import com.bol.mancala.game.service.model.GameResponse;
import com.bol.mancala.game.service.model.NewGameRequest;
import com.bol.mancala.game.service.service.impl.GameService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Objects;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class ApiExceptionHandlerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private GameService gameService;

    @Test
    void itShouldHandleMethodArgumentNotValidException() throws Exception {
        //Given new game request model
        NewGameRequest newGameRequest = NewGameRequest.builder()
                .playerNorthName("hakan")
                .playerSouthName("Johnannes el mundo del piero dos santos junior")
                .build();

        //When
        ResultActions gameCreateAction = mockMvc.perform(post("/api/v1/game/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content(Objects.requireNonNull(objectToJson(newGameRequest))));
        //Then
        gameCreateAction
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.errorMessage").value("Validation Error"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.httpStatus").value("BAD_REQUEST"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.subErrorList[0].errorMessage").value("south player name field supports min 3 max 25 characters"));
    }

    @Test
    void itShouldHandleEntityNotFoundException() throws Exception {
        //Given new game request model
        UUID gameId = UUID.randomUUID();
        //When
        ResultActions gameCreateAction = mockMvc.perform(get("/api/v1/game/" + gameId));
        //Then
        gameCreateAction
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.errorMessage")
                        .value(String.format("Game Not Found gameId : %s", gameId)));
    }

    @Test
    void itShouldHandleMethodArgumentTypeMismatchException() throws Exception {
        //Given new game request model
        String gameId = "123";
        //When
        ResultActions gameCreateAction = mockMvc.perform(get("/api/v1/game/" + gameId));
        //Then
        gameCreateAction
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.errorMessage")
                        .value(String.format("'game-id' should be a valid 'UUID' and '%s' isn't", gameId)));
    }

    @Test
    void itShouldHandleIllegalMoveException() throws Exception {
        //Given new game request model
        NewGameRequest newGameRequest = NewGameRequest.builder()
                .playerNorthName("Hakan")
                .playerSouthName("John")
                .build();
        // ... new game by service
        GameResponse game = gameService.createGame(newGameRequest);
        //When
        ResultActions gameCreateAction =
                mockMvc.perform(put(String.format("/api/v1/game/%s/pit/105", game.getGameId())));
        //Then
        gameCreateAction
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.errorMessage")
                        .value("moveBeans.pitId: pitId can be max : " + Board.PIT_END_INDEX));
    }

    @Test
    void itShouldHandleConstraintViolationException() throws Exception {
        //Given new game request model
        NewGameRequest newGameRequest = NewGameRequest.builder()
                .playerNorthName("Hakan")
                .playerSouthName("John")
                .build();
        // ... new game by service
        GameResponse game = gameService.createGame(newGameRequest);
        //When
        ResultActions gameCreateAction =
                mockMvc.perform(put(String.format("/api/v1/game/%s/pit/7", game.getGameId())));
        //Then
        gameCreateAction
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.errorMessage")
                        .value("Hop ! Can not play from home pit"));
    }

    private String objectToJson(Object object) {
        try {
            return new ObjectMapper().writeValueAsString(object);
        } catch (JsonProcessingException e) {
            fail("Failed to convert Object to JSON");
            return null;
        }
    }
}
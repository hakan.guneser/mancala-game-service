package com.bol.mancala.game.service.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PlayerTest {

    @Test
    void itShouldGetHomePit() {
        final Player playerNorth = Player.PLAYER_NORTH;
        final Player playerSouth = Player.PLAYER_SOUTH;

        int expectedNorthHomePitId = 7;
        int expectedSouthHomePitId = 14;

        assertEquals(expectedNorthHomePitId, playerNorth.getHomePit());
        assertEquals(expectedSouthHomePitId, playerSouth.getHomePit());
    }
}
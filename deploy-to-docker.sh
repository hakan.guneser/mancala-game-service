#!/bin/sh

mvn clean install

docker build --tag=mancala-game-service:latest .

docker-compose  -f ./src/main/resources/docker-compose.yml up -d
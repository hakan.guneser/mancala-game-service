FROM openjdk:17-oracle
MAINTAINER hakan.guneser
COPY target/mancala-game-service-1.0.0.jar mancala-game-service-1.0.0.jar
ENTRYPOINT ["java","-jar","/mancala-game-service-1.0.0.jar"]